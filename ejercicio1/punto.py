import math


class Punto:

    def __init__(self,x,y,xpol,ypol):
        self.__x = x
        self.__y = y
        self.__xpol = xpol
        self.__ypol = ypol


    def getx(self):
        return self.__x

    def setx(self,x):
        self.__x = x

    def gety(self):
        return self.__y

    def sety(self,y):
        self.__y = y

    def getxpol(self):
        return self.__xpol

    def getypol(self):
        return self.__ypol

    def __str__(self):
        return ("Las coordenadas son en x e y: ({};{}), y sus coordenadas polares en x e y: ({};{})").format(self.__x,self.__y,self.__xpol,self.__ypol)