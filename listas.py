"""Ejercicio de Registrar, listar y buscar alumno """

"""------------------------
[1]. Registrar Alumno
[2]. Listar Alumno
[3]. Buscar Alumno
[4]. Salir
----------------------------"""

class Alumno:

    def __init__(self,rut,nombre,apellido,edad):
        self.__rut = rut
        self.__nombre = nombre
        self.__apellido = apellido
        self.__edad = edad

    def getrut(self):
        return self.__rut

    def setrut(self,rut):
        self.__rut = rut

    def getnombre(self):
        return self.__nombre

    def setnombre(self,nombre):
        self.__nombre = nombre

    def getapell(self):
        return self.__apellido

    def setapell(self,apellido):
        self.__apellido = apellido

    def getedad(self):
        return self.__edad

    def setedad(self,edad):
        self.__edad = edad
        


    def __str__(self):
        return ("Rut: {}, Nombre: {}, Apellido: {}, Edad: {}".format(self.__rut,self.__nombre,self.__apellido,self.__edad))





