import random

class Carton():
    

    def __init__(self,id,size):
        
        self.id = id
        self.size = size
        self.__numerosRestantes = size
        self.__carton = [random.randint(1,101) for i in range(1, self.size + 1)]
        self.__origin = self.__carton.copy()
        print(self.__origin)
        
    def push(self,x):
        return self.__carton.append(x)


    def getId(self):
        return self.id

    def getcarton(self, origin=False):

        carton=self.__origin if origin else self.__carton

        for i in range(0, len(carton), 5):
            print(carton[i:i+5])


    def marcarNumero(self, numeroMarcar):
        if numeroMarcar in self.__carton:
            self.__carton[self.__carton.index(numeroMarcar)]="X"
            self.__numerosRestantes-=1
            return True
        return False

    def getnumres(self):
        return self.__numerosRestantes
            
        

    def __str__(self):
        return ("Carton1: {}".format(self.__carton))

